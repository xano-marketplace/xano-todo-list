import { Component, EventEmitter, OnInit } from '@angular/core';
import { PanelComponent } from '../panel/panel.component';
import { PanelService } from '../panel.service';
import { FormService } from '../form.service';
import { ConfigService } from '../config.service';
import { Validators } from '@angular/forms';
import { ApiService } from '../api.service';
import { XanoService } from '../xano.service';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-todo-manage',
  templateUrl: './todo-manage.component.html',
  styleUrls: ['./todo-manage.component.scss']
})
export class TodoManageComponent extends PanelComponent {
  static ID = Symbol();
  todoForm;
  saving;

  constructor(
    protected panel: PanelService,
    public config: ConfigService,
    protected form: FormService,
    protected api: ApiService,
    protected toast: ToastService,
    protected xano: XanoService
  ) { 
    super(panel);
  }

  getTitle() {
    return this.data.item ? "Edit Todo" : "Add Todo";
  }

  getDescription() {
    return "Configure your task below";
  }

  getLabel() {
    return this.data.item ? "Update" : "Add";
  }

  onOpen() {
    this.todoForm = this.createForm();
  }

  createForm() {
    let defaultValue = this.data.item || {};

    return this.form.createFormGroup({
      obj: {
        task: ['text', [Validators.required]],
        important: 'bool',
        completed: 'bool',
      },
      defaultValue
    });
  }

  getComponentId() {
    return TodoManageComponent.ID;
  }

  static open(panel: PanelService, args: {item:any, successEmitter?: EventEmitter<any>, deleteEmitter?: EventEmitter<any>}) {
    panel.open(this.ID, 'Todo', {
      ...args
    });
  }

  getEndpoint() {
    return this.data.item ? `${this.config.xanoApiUrl}/todo/${this.data.item.id}` : `${this.config.xanoApiUrl}/todo`
  }

  delete() {
    this.api.delete({
      endpoint: this.getEndpoint(),
      stateFunc: state => this.saving = state
    }).subscribe((res:any) => {
      if (this.data.deleteEmitter) {
        this.data.deleteEmitter.emit(res);
      }
      this.close();
    }, err => {
      this.toast.error("An error has occured.");
    });
  }

  save() {
    if (!this.todoForm.trySubmit()) return;

    this.api.post({
      endpoint: this.getEndpoint(),
      params: this.todoForm.value,
      stateFunc: state => this.saving = state
    }).subscribe((res:any) => {
      if (this.data.successEmitter) {
        this.data.successEmitter.emit(res);
      }
      this.close();
    }, err => {
      this.toast.error("An error has occured.");
    });
  }
}
