import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  title = "Todo List";
  summary = "This extension provides a fully functioning demo to manage a personal todo list.";
  editText = "Get source code";
  editLink = "https://gitlab.com/xano-marketplace/xano-todo-list";
  descriptionHtml = `
  <h4>Description</h4>
  <p>This demo consists of following components:</p>
  <h5>Personal View</h5>
  <p>This is your standard todo list view where you see a list of tasks. Each task has some functionality to edit the task, mark it important, or complete it.</p>
  <h5>Manage View</h5>
  <p>This is a side panel that lets you add a new todo task or edit an existing one. The edit component also supports deleting a task.</p>
  <h5>Search</h5>
  <p>A search box is present to allow you to search content from with each task description. There is also support for filtering based on completed and important attributes.</p>
  `;
  logoHtml = '';

  requiredApiPaths = [
    '/todo',
    '/todo/{id}'
  ];

  xanoApiUrl;

  constructor() { }

  isConfigured() {
    return !!this.xanoApiUrl;
  }
}
