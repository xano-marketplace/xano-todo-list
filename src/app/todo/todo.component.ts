import { Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { ConfigService } from '../config.service';
import { ToastService } from '../toast.service';
import * as _ from 'lodash';
import { FormGroup, NgModel, Validators } from '@angular/forms';
import { FormService } from '../form.service';
import { faSpinner, faExclamation, faSearch } from '@fortawesome/free-solid-svg-icons';
import { TodoManageComponent } from '../todo-manage/todo-manage.component';
import { PanelService } from '../panel.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  form;
  searchForm;
  success;
  loading;
  saving: boolean;
  todos = [];

  spinner = faSpinner;
  exclamation = faExclamation;
  search = faSearch;

  currencies = true;

  constructor(
    private api: ApiService,
    private toast: ToastService,
    protected formService: FormService,
    protected panel: PanelService,
    public config: ConfigService
  ) { }

  ngOnInit(): void {
    this.form = this.createForm();

    this.searchForm = this.createSearchForm();

    this.searchForm.valueChanges
    .pipe(
      debounceTime(300),
      distinctUntilChanged(),
    )
    .subscribe(() => {
      this.getTodoData()
    });

    this.getTodoData();
  }

  addTodo() {
    let onSuccess = new EventEmitter;
    onSuccess.subscribe(payload => {
      this.toast.success("Todo added.")
      this.getTodoData();
    });

    TodoManageComponent.open(this.panel, {
      item: null,
      successEmitter: onSuccess
    });
  }

  editTodo(item) {
    let onSuccess = new EventEmitter;
    onSuccess.subscribe(payload => {
      this.toast.success("Todo updated.")
      this.getTodoData();
    });

    let onDelete = new EventEmitter;
    onDelete.subscribe(payload => {
      this.toast.success("Todo deleted.")
      this.getTodoData();
    });

    TodoManageComponent.open(this.panel, {
      item,
      successEmitter: onSuccess,
      deleteEmitter: onDelete
    });
  }

  updateCompleted(item) {
    this.saveTodo(item);
  }

  saveTodo(item) {
    this.api.post({
      endpoint: `${this.config.xanoApiUrl}/todo/${item.id}`,
      params: item,
    }).subscribe((res:any) => {
      this.toast.success("Todo updated.");
    }, err => {
      this.toast.error("An error has occured.");
    });
  }

  getTodoData() {
    let search = {
      expression: []
    };

    let searchValue = this.searchForm.value.search.trim();

    if (searchValue) {
      search.expression.push({
        statement: {
          left: {
            tag: "col",
            operand: "todo.task",
          },
          op: "ilike",
          right: {
            operand: `%${searchValue}%`,
          }
        }
      });
    }

    if (this.searchForm.value.completed) {
      search.expression.push({
        statement: {
          left: {
            tag: "col",
            operand: "todo.completed",
          },
          right: {
            operand: true,
          }
        }
      });
    }

    if (this.searchForm.value.important) {
      search.expression.push({
        statement: {
          left: {
            tag: "col",
            operand: "todo.important",
          },
          right: {
            operand: true,
          }
        }
      });
    }

    this.api.get({
      endpoint: `${this.config.xanoApiUrl}/todo`,
      params: {
        search
      },
      stateFunc: state => this.loading = state
    })
    .subscribe((response:any) => {
      this.todos = response;
    }, (err) => {
      this.toast.error(_.get(err, "error.message") || "An unknown error has occured.");
    });
  }

  createForm() {
    return this.formService.createFormGroup({
      obj: {
        name: ['text', [Validators.required]],
        email: ['email', [Validators.required, Validators.email]],
        company: ['text', []],
        request: ['text', [Validators.required]],
      }
    });
  }

  createSearchForm() {
    return this.formService.createFormGroup({
      obj: {
        search: 'text',
        important: 'bool',
        completed: 'bool'
      },
      defaultValue: {
        search: "",
        important: false,
        completed: false
      }
    });
  }

  // submit() {
  //   if (!this.form.trySubmit()) return;

  //   this.api.post({
  //     endpoint: `${this.config.xanoApiUrl}/lead`,
  //     params: {
  //       ...this.form.value
  //     },
  //     stateFunc: state => this.saving = state
  //   })
  //   .subscribe((response:any) => {
  //     this.toast.success("Success");
  //     this.success = true;
  //     this.setActiveTab("list");
  //   }, (err) => {
  //     this.toast.error(_.get(err, "error.message") || "An unknown error has occured.");
  //   });
  // }
}
